// import { useContext } from "react"
// import Context from "../Context";
// import Button from "../button/Button";
import windowStyles from "./Modal.module.scss";

export function ModalOne({ className, closeButton, headline="", text="" }) {
  return (
    <div className={className}>
      <div className={windowStyles.headlineWrapper}>
        <h2 className={windowStyles.headline}>{headline}</h2>
        <div onClick={closeButton} className={windowStyles.cross}></div>
      </div>
      <p className={windowStyles.text}>{text}</p>
    </div>
  );
}

// export function ModalTwo({ className, closeButton, headline="", text="" }) {
//   const {starHandler} = useContext(Context);

//   return (
//     <div className={className}>
//       <div className={windowStyles.headlineWrapper}>
//         <h2 className={windowStyles.headline}>{headline}</h2>
//         <div onClick={closeButton} className={windowStyles.cross}></div>
//       </div>
//       <p className={windowStyles.text}>{text}</p>
//       <div className={windowStyles.windowButtonWrapperTwo}>
//         <Button
//           className={`${windowStyles.button} ${windowStyles.windowButton}`}
//           text="Додати"
//           onClick={starHandler}
//         />
//         <Button
//             className={`${windowStyles.button} ${windowStyles.windowButton}`}
//             text="Відмінити"
//             onClick={closeButton}
//           />
//       </div>
//     </div>
//   );
// }
