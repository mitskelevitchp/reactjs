import React, { useEffect } from "react";
import { useSelector, useDispatch} from 'react-redux'
import { getProductsList, changeStar, changeBusket } from '../../stores/actions'
import { Card } from "../card/Card";
import styles from "./CardsList.module.scss";



export default function CardsList() {

    const state = useSelector((state) => state.productsList);
    const dispatch = useDispatch();

    useEffect(() => {
        fetch("./products/products.json").then(resp => resp.json()).then(data => {
            dispatch(getProductsList(data))
        })
    }, [dispatch]);


    const handleStarClick = (productId) => {
        dispatch(changeStar(productId));
    };

    const handleBusketClick = (productId) => {
        dispatch(changeBusket(productId));
    };




    return (
        <div className={styles.contentWrapper}>
            <h2 className={styles.contentHeadline}>Магазин відверто вживаної техніки, що ще дихає</h2>
            <p>Культові телефони, які пам'ятають тепло людських рук. Кожен з них залишав по собі лише добрі емоції своєю якісною збіркою та гарним рівнем прийому сигналу. Комплектація моделей повна, включає зарядний пристрій, інструкцію з експлуатації на чистій мандаринській китайській та оригінально покоцану коробку.</p>
            <ul className={styles.list}>
                {state.map((product, index) => (
                    <Card
                    key={index}
                    product={product}
                    onStarClick={() => handleStarClick(product.id)}
                    onButtonClick={() => handleBusketClick(product.id)}
                    />
                ))}
            </ul>
        </div>
    );
}