import React, { useState, useContext } from "react";
import Context from "../Context";
import { Link } from "react-router-dom";
import { CardBusketed } from "../card/Card";
import styles from "../cardsList/CardsList.module.scss";



export default function Busket() {
    const { handleBusketClickMinus } = useContext(Context);
    const [products, setProducts] = useState(JSON.parse(localStorage.getItem("Products in the Busket")) || []);

    // find the right product by its index: findIndex + splice
    const handleDelete = (id) => {
        const indexToDelete = products.findIndex(product => product.id === id);
        if (indexToDelete !== -1) {
            const updatedProducts = [...products];
            updatedProducts.splice(indexToDelete, 1);

            setProducts(updatedProducts);
            localStorage.setItem("Products in the Busket", JSON.stringify(updatedProducts));
            handleBusketClickMinus();
        }
    };
      

    return (
        <div className={styles.contentWrapper}>
            <Link to="/">Головна</Link>
            <span className={styles.arrow}>&gt;</span>
            <span>Кошик</span>
            <h2 className={styles.contentHeadline}>Товари в кошику:</h2>
            <div className={styles.productsSection}>
                <ul className={styles.list} >
                    {products.map((product, index) => (
                        <CardBusketed
                        key={index}
                        product={product}
                        deleteFromBusket={handleDelete}
                        />
                    ))}
                </ul>
            </div>
        </div>
    );
}
