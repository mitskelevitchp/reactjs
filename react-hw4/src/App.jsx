import React, { useState, useEffect, useCallback } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom"
import { useSelector, useDispatch } from 'react-redux'
import { changeModalStatus, changeDeleteModalStatus } from './stores/actions'
import Header from "./components/header/Header";
import Main from "./components/main/Main";
import CardsList from "../src/components/cardsList/CardsList";
import SelectedProducts from "./components/selectedProducts/SelectedProducts"
import Busket from "./components/busket/Busket";
import Context from "./components/Context";
import { ModalOne } from "./components/modals/Modal";
import appStyles from "./App.module.scss";
import headerStyles from "./components/header/Header.module.scss";
import mainStyles from "./components/main/Main.module.scss";
import windowStyles from "./components/modals/Modal.module.scss";




function App() {

  const [isOverlayOpen, setOverlayOpen] = useState(false);
  const [autoCloseModal, setAutoCloseModal] = useState(null);
  const [coloredStarsCount, setColoredStarsCount] = useState( parseInt(localStorage.getItem("coloredStarsCount")) || 0 );
  const [busketCount, setbusketCount] = useState( parseInt(localStorage.getItem("busketCount")) || 0 );

  const dispatch = useDispatch();
  const state = useSelector((state) => state);


  // open window
  const openModal = () => {
    dispatch(changeModalStatus(true));
    setOverlayOpen(true);
    const timer = setTimeout(closeModal, 2000);
    setAutoCloseModal(timer);
  };

  const openDeleteModal = () => {
    dispatch(changeDeleteModalStatus(true));
    setOverlayOpen(true);
    const timer = setTimeout(closeModal, 2000);
    setAutoCloseModal(timer);
  };

  // close window
  const closeModal = useCallback(() => {
    dispatch(changeModalStatus(false));
    dispatch(changeDeleteModalStatus(false));
    setOverlayOpen(false);
    if (autoCloseModal) {
      clearTimeout(autoCloseModal);
      setAutoCloseModal(null);
    }
  }, [autoCloseModal, dispatch]);

  const toggleWindow = () => {
    setOverlayOpen(!isOverlayOpen);
  };


  useEffect(() => {
    localStorage.setItem("coloredStarsCount", coloredStarsCount);
    localStorage.setItem("busketCount", busketCount);

    const handleOutsideClick = (event) => {
      if (((state.isModalOpen || state.isModalDeleteOpen) && (!event.target.closest(".window-one") || !event.target.closest(".window-two")))) {
        closeModal();
      }
    };

    document.addEventListener("click", handleOutsideClick);

    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, [closeModal, coloredStarsCount, busketCount, state.isModalOpen, state.isModalDeleteOpen]);

  // 
  // Stars counter + Buskets counter
  const handleStarClick = (productId, isColored) => {
    if (isColored) {
      setColoredStarsCount((prevCount) => prevCount + 1);
    } else {
      return
    }
  };

  const handleStarClickMinus = (productId, isColored) => {
    if (!isColored) {
      setColoredStarsCount((prevCount) => prevCount - 1);
    } else {
      return
    }
  };

  const handleBusketClick = (productId, isButtonClicked) => {
      setbusketCount((prevCount) => prevCount + 1);
  };

  const handleBusketClickMinus = (productId, isButtonClicked) => {
    setbusketCount((prevCount) => prevCount - 1);
  };

  // 
  // WindowContext
  const contextDataMOdal = { openModal, openDeleteModal, coloredStarsCount, handleStarClick, handleStarClickMinus, handleBusketClick, handleBusketClickMinus, busketCount }




  return (
    <div className={appStyles.container}>
      <BrowserRouter>
        <Context.Provider value={contextDataMOdal}>

          {isOverlayOpen && (<div className={appStyles.overlay} onClick={toggleWindow} />)}
          <div className={(state.isModalOpen || state.isModalDeleteOpen) ? headerStyles.headerHidden : headerStyles.headerWrapper}>
            <Header />
          </div>

          <Main className={state.isModalOpen || state.isModalDeleteOpen ? mainStyles.mainHidden : mainStyles.main}>
              {state.isModalOpen && (
                <ModalOne className={`window-one ${windowStyles.window} ${windowStyles.windowOne}`} closeButton={closeModal} text="Додано до кошика" />
              )}
              {state.isModalDeleteOpen && (
                <ModalOne className={`window-two ${windowStyles.window} ${windowStyles.windowTwo}`} closeButton={closeModal} text="Видалено з кошика" />
              )}

            <Routes>
              <Route path="/" element={<CardsList />}/>
              <Route path="/selected" element={<SelectedProducts/>}/>
              <Route path="/busket" element={<Busket />}/>
            </Routes>
            
          </Main>
          <footer className={appStyles.footer}>
            <p>© OLDphones 2023</p>
          </footer>

        </Context.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
