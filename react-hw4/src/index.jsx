import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from 'react-redux';
import { mainStore } from './stores/mainStore'
import App from "./App";
import "./index.scss";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <Provider store = { mainStore }>
        <App />
    </Provider>
);