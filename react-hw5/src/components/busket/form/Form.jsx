import { Form, Field, ErrorMessage, Formik, useField } from 'formik'
import React, { useContext } from "react";
import Context from "../../Context";
import { object, string, number } from 'yup'
import { PatternFormat } from 'react-number-format';
import styles from "./Form.module.scss"
import buttonStyles from "../../button/Button.module.scss"


const validationSchema = object().shape({
    fname: string()
        .min(3, "Некоректні дані")
        .matches(/^[a-zA-Zа-яА-Яії'є]*$/, "Некоректні дані")
        .required('Заповніть поле'),
    lname: string()
        .min(3, "Некоректні дані")
        .matches(/^[a-zA-Zа-яА-Яії'є]*$/, "Некоректні дані")
        .required('Заповніть поле'),
    age: number()
        .integer('Вік має бути цілим числом')
        .min(16, 'Вам має бути 16+')
        .max(122, '122 - сітовий рекорд')
        .required('Заповніть поле')
        .typeError('Введіть число'),
    adress: string()
        .required('Заповніть поле'),
    phone: string()
        .min(10, 'Невірний формат')
        .required('Заповніть поле'),
})

const PhoneInput = ({ name, ...props }) => {
    const [field, meta, helpers] = useField(name);
  
    return (
      <div className={styles.phoneInputWrapper}>
        <PatternFormat
          value={field.value}
          onValueChange={({ value }) => {
            helpers.setValue(value);
          }}
          className={styles.phoneInput}
          format="+38 (###) ### ## ##"
          allowEmptyFormatting
          mask="#"
          {...props}
        />
        {meta.touched && meta.error ? (
          <p className={styles.phoneErrorMessage}>{meta.error}</p>
        ) : null}
      </div>
    );
  };


export default function BusketForm(props) {

    const { setbusketCount } = useContext(Context);

    const formSubmitHandler = (values, { resetForm }) => {
        let resetCount = 0;
        let lsData = localStorage.getItem("Products in the Busket");
        let products = JSON.parse(lsData);

        console.log(`Нове замовлення. Покупець: ${values.fname} ${values.lname}, вік: ${values.age}, контактний телефон: ${values.phone}, на адресу: ${values.adress}. Замовлення:`);
        const order = [];
        products.forEach(item => order.push(`Артикул: ${item.article}, модель: ${item.name}, колір: ${item.color}, ціна: ${item.price}`));
        console.log(order);
        let sumArray = [];
        products.forEach(item => sumArray.push(item.price));
        let totalPrice = sumArray.reduce((sum, current) => sum + current, 0);
        console.log(`Всього на суму: ${totalPrice}`);

        localStorage.setItem("Products in the Busket", JSON.stringify([]));
        localStorage.setItem("busketCount", JSON.stringify(0));
        setbusketCount(resetCount);
        props.productsReset([]);
        resetForm();
    }

    return ( 
        <Formik
        initialValues={{
            fname: '',
            lname: '',
            age: '',
            adress: '',
            phone: ''
        }}
            onSubmit={formSubmitHandler}
            validationSchema={validationSchema}
        >   
            <Form className={styles.form}>
                <div className={styles.inputWrapper}>
                    <p className={styles.text}>Ім'я:</p>
                    <Field name="fname"
                    className={styles.input}
                    />
                    <ErrorMessage name='fname' component="p" className={styles.nameErrorMessage} />
                </div>

                <div className={styles.inputWrapper}>
                    <p className={styles.text}>Прізвище:</p>
                    <Field name="lname" className={styles.input}/>
                    <ErrorMessage name='lname' component="p" className={styles.nameErrorMessage} />
                </div>

                <div className={styles.inputWrapper}>
                    <p className={styles.text}>Вік:</p>
                    <Field name="age" className={styles.input}/>
                    <ErrorMessage name='age' component="p" className={styles.nameErrorMessage} />
                </div>

                <div className={styles.inputWrapper}>
                    <p className={styles.text}>Адреса:</p>
                    <Field name="adress" className={styles.input}/>
                    <ErrorMessage name='adress' component="p" className={styles.nameErrorMessage} />
                </div>

                <div className={styles.inputWrapper}>
                    <p className={styles.text}>Телефон:</p>
                    <PhoneInput name="phone" />
                </div>
                <div className={styles.buttonWrapper}>
                    <button type='submit' className={`${buttonStyles.button} ${styles.button}`}>Замовити</button>
                </div>
            </Form>
        </Formik>
     )
}