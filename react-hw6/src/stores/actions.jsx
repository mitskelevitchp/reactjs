export const GET_PRODUCTS_LIST = "GET_PRODUCTS_LIST"
export const CHANGE_STAR = "CHANGE_STAR"
export const CHANGE_BUSKET = "CHANGE_BUSKET"
export const STATUS_MODAL_WINDOW = "STATUS_MODAL_WINDOW"
export const STATUS_DELETE_MODAL_WINDOW = "STATUS_DELETE_MODAL_WINDOW"


export const getProductsList = (productsList) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: GET_PRODUCTS_LIST,
                payload: productsList
            });
            
        } catch (error) {
            console.error("Async action getProductsList error:", error);
        }
    };
}

export const changeStar = (productId) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: CHANGE_STAR,
                payload: productId
            });
            
        } catch (error) {
            console.error("Async action changeStar error:", error);
        }
    };
}

export const changeBusket = (productId) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: CHANGE_BUSKET,
                payload: productId
            });
            
        } catch (error) {
            console.error("Async action changeBusket error:", error);
        }
    };
}

export const changeModalStatus = (isOpen) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: STATUS_MODAL_WINDOW,
                payload: isOpen
            });
            
        } catch (error) {
            console.error("Async action changeModalStatus error:", error);
        }
    };
}

export const changeDeleteModalStatus = (isOpen) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: STATUS_DELETE_MODAL_WINDOW,
                payload: isOpen
            });
            
        } catch (error) {
            console.error("Async action changeDeleteModalStatus error:", error);
        }
    };
}