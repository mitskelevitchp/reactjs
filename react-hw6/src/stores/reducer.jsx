import { GET_PRODUCTS_LIST, CHANGE_STAR, CHANGE_BUSKET, STATUS_MODAL_WINDOW, STATUS_DELETE_MODAL_WINDOW } from "../stores/actions"

export const defaultState = {
    productsList: [],
    isModalOpen: false,
    isModalDeleteOpen: false
}

export function defaultReducer(state = defaultState, action){
    switch (action.type) {

        case GET_PRODUCTS_LIST:
            return {...state, productsList:[...action.payload]}

        case CHANGE_STAR:
            return {
                ...state,
                productsList: state.productsList.map(product => 
                    product.id === action.payload
                        ? {...product, isColored: !product.isColored}
                        : product
                )
            };

        case CHANGE_BUSKET:
            return {
                ...state,
                productsList: state.productsList.map(product => 
                    product.id === action.payload
                        ? {...product, isBusketClicked: !product.isBusketClicked}
                        : product
                )
            };

        case STATUS_MODAL_WINDOW:
            return {...state, isModalOpen: action.payload};

        case STATUS_DELETE_MODAL_WINDOW:
            return {...state, isModalDeleteOpen: action.payload}

        default:
            return state
    }
}