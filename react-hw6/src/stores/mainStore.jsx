import { defaultReducer } from './reducer'
import { configureStore } from '@reduxjs/toolkit'

export const mainStore = configureStore({
    reducer: defaultReducer
})