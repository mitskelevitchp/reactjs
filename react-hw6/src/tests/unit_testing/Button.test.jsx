import { render, screen } from "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import Button from "../../components/button/Button"

test("check button exist", () => {
    render(<Button />);
    const buttonComponent = screen.getByRole("button");
    expect(buttonComponent).toBeInTheDocument()
})