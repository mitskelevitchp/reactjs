import { render, screen } from "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import Modal from "../../components/modals/Modal"

test("check modale window exist", () => {
    render(<Modal />);
    const modalComponent = screen.getByTestId("modal-container");
    expect(modalComponent).toBeInTheDocument()
})