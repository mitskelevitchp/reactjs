import { defaultReducer } from '../../stores/reducer';
import { GET_PRODUCTS_LIST, CHANGE_STAR, CHANGE_BUSKET, STATUS_MODAL_WINDOW, STATUS_DELETE_MODAL_WINDOW } from "../../stores/actions"

describe('defaultReducer', () => {
  it('must process the action GET_PRODUCTS_LIST', () => {
    const defaultState = {
      productsList: [],
      isModalOpen: false,
      isModalDeleteOpen: false,
    };

    const action = {
      type: GET_PRODUCTS_LIST,
      payload: [{ id: 1, name: 'Product 1' }],
    };

    const newState = defaultReducer(defaultState, action);

    expect(newState.productsList).toEqual(action.payload);
  });

  it('must process the action CHANGE_STAR', () => {
    const defaultState = {
      productsList: [{ id: 1, isColored: false }],
      isModalOpen: false,
      isModalDeleteOpen: false,
    };

    const action = {
      type: CHANGE_STAR,
      payload: 1,
    };

    const newState = defaultReducer(defaultState, action);

    expect(newState.productsList[0].isColored).toBe(true);
  });

  it('must process the action CHANGE_BUSKET', () => {
    const defaultState = {
      productsList: [{ id: 1, isBusketClicked: false }],
      isModalOpen: false,
      isModalDeleteOpen: false,
    };

    const action = {
      type: CHANGE_BUSKET,
      payload: 1,
    };

    const newState = defaultReducer(defaultState, action);

    expect(newState.productsList[0].isBusketClicked).toBe(true);
  });

  it('must process the action STATUS_MODAL_WINDOW', () => {
    const defaultState = {
      productsList: [],
      isModalOpen: false,
      isModalDeleteOpen: false,
    };

    const action = {
      type: STATUS_MODAL_WINDOW,
      payload: true,
    };

    const newState = defaultReducer(defaultState, action);

    expect(newState.isModalOpen).toBe(true);
  });

  it('must process the action STATUS_DELETE_MODAL_WINDOW', () => {
    const initialState = {
      productsList: [],
      isModalOpen: false,
      isModalDeleteOpen: false,
    };

    const action = {
      type: STATUS_DELETE_MODAL_WINDOW,
      payload: true,
    };

    const newState = defaultReducer(initialState, action);

    expect(newState.isModalDeleteOpen).toBe(true);
  });
});
