import testRenderer from "react-test-renderer"
import { IconCardList } from "../../components/cardsList/icons/ListIcons.jsx"

test("icons of the products list", () => {
    const iconsComponent = testRenderer.create(<IconCardList/>);
    const structure = iconsComponent.toJSON();
    expect(structure).toMatchSnapshot();
})