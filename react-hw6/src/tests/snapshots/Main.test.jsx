import testRenderer from "react-test-renderer"
import Main from "../../components/main/Main"

test("main component", () => {
    const mainComponent = testRenderer.create(<Main/>);
    const structure = mainComponent.toJSON();
    expect(structure).toMatchSnapshot();
})