import testRenderer from "react-test-renderer"
import Button from "../../components/button/Button";

test("button component", () => {
    const buttonComponent = testRenderer.create(<Button/>);
    const structure = buttonComponent.toJSON();
    expect(structure).toMatchSnapshot();
})