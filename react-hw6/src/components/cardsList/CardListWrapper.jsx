import React, { useContext } from "react";
import Context from "../Context";
import { IconCardList } from "./icons/ListIcons"
import CardList from "../cardsList/CardsList";
import styles from "./CardsList.module.scss";



export default function CardsListWrapper() {

    const { typeOfList, setTypeOfList } = useContext(Context);

    function typeIconHandler() {
        setTypeOfList(prev => !prev);
      }
    
    return (
        // <Context.Provider value={typeOfList}>

            <div className={styles.contentWrapper}>
                <h2 className={styles.contentHeadline}>Магазин відверто вживаної техніки, що ще дихає</h2>
                <p>Культові телефони, які пам'ятають тепло людських рук. Кожен з них залишав по собі лише добрі емоції своєю якісною збіркою та гарним рівнем прийому сигналу. Комплектація моделей повна, включає зарядний пристрій, інструкцію з експлуатації на чистій мандаринській китайській та оригінально покоцану коробку.</p>
                <div className={styles.iconsWrapper}>
                    <IconCardList typeOfList={typeOfList} typeIconHandler={typeIconHandler} />
                </div>
                <CardList />
            </div>

        // </Context.Provider>

    );
}