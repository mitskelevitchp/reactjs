import React, { useEffect, useContext } from "react";
import Context from "../Context";
import { useSelector, useDispatch} from 'react-redux'
import { getProductsList, changeStar, changeBusket } from '../../stores/actions'
import { Card } from "../card/Card";
import styles from "./CardsList.module.scss";



export default function CardsList() {

    const { typeOfList } = useContext(Context);


    const state = useSelector((state) => state.productsList);
    const dispatch = useDispatch();

    useEffect(() => {
        fetch("./products/products.json").then(resp => resp.json()).then(data => {
            dispatch(getProductsList(data))
        })
    }, [dispatch]);


    const handleStarClick = (productId) => {
        dispatch(changeStar(productId));
    };

    const handleBusketClick = (productId) => {
        dispatch(changeBusket(productId));
    };




    return (
        typeOfList ? 
        <ul className={styles.listList}>
            {state.map((product, index) => (
                <Card
                key={index}
                product={product}
                onStarClick={() => handleStarClick(product.id)}
                onButtonClick={() => handleBusketClick(product.id)}
                />
            ))}
        </ul>
        :
        <ul className={styles.listTile}>
            {state.map((product, index) => (
                <Card
                key={index}
                product={product}
                onStarClick={() => handleStarClick(product.id)}
                onButtonClick={() => handleBusketClick(product.id)}
                />
            ))}
        </ul>
    );
}