import React from "react";
// import { Context as IconListContext } from "../Context";
import { ReactComponent as IconListSVG } from './list.svg';
import { ReactComponent as IconCardsSVG } from './cards.svg';

const IconList = () => {
  return <IconListSVG />;
};

const IconCards = () => {
  return <IconCardsSVG />;
};



function IconCardList(props) {

  return (
    <div onClick={props.typeIconHandler}>
      {props.typeOfList ? <IconCards /> : <IconList />}
    </div>
  )
}
  
export { IconCardList };