import React, { useState, useContext } from "react";
import Context from "../Context";
import { useDispatch} from 'react-redux'
import { changeBusket } from '../../stores/actions'
import { Link } from "react-router-dom";
import { CardSelected } from "../card/Card";
import styles from "../cardsList/CardsList.module.scss";


export default function SelectedProducts() {
    const { handleStarClickMinus } = useContext(Context);
    const [products, setProducts] = useState(JSON.parse(localStorage.getItem("Selected products")) || []);

    const dispatch = useDispatch();

    const handleDelete = (id) => {
        const updatedProducts = products.filter(product => product.id !== id);
        setProducts(updatedProducts);
        localStorage.setItem("Selected products", JSON.stringify(updatedProducts));
        handleStarClickMinus();
      };

    const handleBusketClick = (productId) => {
        dispatch(changeBusket(productId));
    };

    return (
        <div className={styles.contentWrapper}>
            <Link to="/">Головна</Link>
            <span className={styles.arrow}>&gt;</span>
            <span>Обрані продукти</span>
            <h2 className={styles.contentHeadline}>Ви обрали:</h2>
            <ul className={styles.listTile}>
                {products.map((product) => (
                    <CardSelected
                    key={product.id}
                    product={product}
                    deleteStar={handleDelete}
                    onButtonClick={() => handleBusketClick(product.id)}
                    />
                ))}
            </ul>
        </div>
    );
}
