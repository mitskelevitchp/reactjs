import React, { useContext, useState, useEffect } from "react";
import Context from "../Context";
import PropTypes from "prop-types"
import { Icon, IconTransparent} from "../header/icons/star/Star";
import Button from "../button/Button";
import styles from "./Card.module.scss";
import busketStyles from "../busket/Busket.module.scss";
import stylesBtn from "../button/Button.module.scss";





function Card({ product, onStarClick, onButtonClick}) {

  const { openModal, handleStarClick, handleBusketClick, typeOfList } = useContext(Context);
  const [selectedProducts, setSelectedProducts] = useState([]);
  

  useEffect(() => {
    const storedProducts = JSON.parse(localStorage.getItem("Selected products")) || [];
    setSelectedProducts(storedProducts.map(item => item.id));
  }, []);

  const isProductColored = selectedProducts.includes(product.id);

  const handleCardStarClick = () => {
    if (!product.isColored) {
      const newIsColored = true;
  
      onStarClick(product.id, newIsColored);
      handleStarClick(product.id, newIsColored);
  
      const currentProducts = JSON.parse(localStorage.getItem("Selected products")) || [];
      const isProductInArray = currentProducts.some((item) => item.id === product.id);
  
      if (!isProductInArray) {
        currentProducts.push({ ...product, isColored: newIsColored });
        localStorage.setItem("Selected products", JSON.stringify(currentProducts));
      }
    }
  };

  const handleCardButtonClick = (event) => {
    event.stopPropagation();
    openModal();
    const newIsBusketed = !product.isBusketClicked;
    onButtonClick(product.id, newIsBusketed);
    handleBusketClick(product.id, newIsBusketed);

    const currentProducts = JSON.parse(localStorage.getItem("Products in the Busket")) || [];

      currentProducts.push(product);
      localStorage.setItem("Products in the Busket", JSON.stringify(currentProducts));
  };

  return (
    typeOfList ? 
    <li className={styles.listListItem}>
      <div className={styles.iconListWrapper} onClick={!isProductColored ? handleCardStarClick : null}>
          {product.isColored || isProductColored ? <Icon /> : <IconTransparent />}
      </div>
      <div className={styles.pictureListWrapper}>
        <img src={product.url} alt={`смартфон ${product.name} фото`} className={styles.pictureList} />
      </div>
      <h2 className={styles.headlineList}>{product.name}</h2>
      <p className={styles.textList}>{`Артикул: ${product.article}`}</p>
      <p className={styles.textList}>{`Колір: ${product.color}`}</p>
      <p className={styles.priceList}> Ціна: <br/> <span className={styles.accent}>{`${product.price}`} грн</span></p>
      <div className={styles.buttonListWrapper}>
        <Button className={`${stylesBtn.button} ${styles.button}`} text={"Додати в кошик"} onClick={handleCardButtonClick} />
      </div>
    </li>

    :

    <li className={styles.listTileItem}>
      <div className={styles.iconTileWrapper} onClick={!isProductColored ? handleCardStarClick : null}>
          {product.isColored || isProductColored ? <Icon /> : <IconTransparent />}
      </div>
      <div className={styles.pictureTileWrapper}>
        <img src={product.url} alt={`смартфон ${product.name} фото`} className={styles.pictureTile} />
      </div>
      <h2 className={styles.headlineTile}>{product.name}</h2>
      <p className={styles.textTile}>{`Артикул: ${product.article}`}</p>
      <p className={styles.textTile}>{`Колір: ${product.color}`}</p>
      <p className={styles.priceTile}> Ціна: <span className={styles.accent}>{`${product.price}`} грн</span></p>
      <div className={styles.buttonTileWrapper}>
        <Button className={`${stylesBtn.button} ${styles.button}`} text={"Додати в кошик"} onClick={handleCardButtonClick} />
      </div>
    </li>
  );
}




function CardSelected({ product, deleteStar, onButtonClick }) {
  const { openModal, handleBusketClick } = useContext(Context);


  const handleCardButtonClick = (event) => {
    event.stopPropagation();
    openModal();

    const newIsBusketed = !product.isBusketClicked;
    onButtonClick(product.id, newIsBusketed);
    handleBusketClick(product.id, newIsBusketed);

    const currentProducts = JSON.parse(localStorage.getItem("Products in the Busket")) || [];

      currentProducts.push(product);
      localStorage.setItem("Products in the Busket", JSON.stringify(currentProducts));
  };

  return (
    <li className={styles.listTileItem}>
      
      <div className={styles.iconTileWrapper} onClick={() => deleteStar(product.id)}>
        <Icon />
      </div>
      <div className={styles.pictureTileWrapper}>
        <img src={product.url} alt={`смартфон ${product.name} фото`} className={styles.pictureTile} />
      </div>
      <h2 className={styles.headlineTile}>{product.name}</h2>
      <p className={styles.textTile}>{`Артикул: ${product.article}`}</p>
      <p className={styles.textTile}>{`Колір: ${product.color}`}</p>
      <p className={styles.priceTile}> Ціна: <span className={styles.accent}>{`${product.price}`} грн</span></p>
      <div className={styles.buttonTileWrapper}>
        <Button className={`${stylesBtn.button} ${styles.button}`} text={"Додати в кошик"} onClick={handleCardButtonClick} />
      </div>
      
    </li>
  );
}





function CardBusketed({ product, deleteFromBusket }) {
  const { openDeleteModal } = useContext(Context);

  const handleCardButtonClick = (event) => {
    event.stopPropagation();
    openDeleteModal();
    deleteFromBusket(product.id);
  };

  return (
    <div>
      <div className={busketStyles.crossBgWrapper}>
        <div className={busketStyles.crossWrapper}>
          <div onClick={handleCardButtonClick} className={busketStyles.cross}></div>
        </div>
      </div>
    
      <li className={busketStyles.item}>
        <div className={styles.pictureTileWrapper}>
          <img src={product.url} alt={`смартфон ${product.name} фото`} className={styles.pictureTile} />
        </div>
        <h2 className={styles.headlineTile}>{product.name}</h2>
        <p className={styles.textTile}>{`Артикул: ${product.article}`}</p>
        <p className={styles.textTile}>{`Колір: ${product.color}`}</p>
        <p className={styles.priceTile}> Ціна: <span className={styles.accent}>{`${product.price}`} грн</span></p>
      </li>

    </div>
  );
}


// 
// PropTypes
Card.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
  }).isRequired
};

export { Card, CardSelected, CardBusketed }