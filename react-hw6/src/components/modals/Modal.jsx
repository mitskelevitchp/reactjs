import windowStyles from "./Modal.module.scss";

export default function Modal({ className, closeButton, headline="", text="" }) {
  return (
    <div className={className} data-testid="modal-container">
      <div className={windowStyles.headlineWrapper}>
        <h2 className={windowStyles.headline}>{headline}</h2>
        <div onClick={closeButton} className={windowStyles.cross}></div>
      </div>
      <p className={windowStyles.text}>{text}</p>
    </div>
  );
}
