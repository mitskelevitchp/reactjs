import React, { useState, useEffect, useCallback } from "react";
import Header from "./components/header/Header";
import Main from "./components/main/Main";
import { WindowContext } from "./WindowsContext";
import { ModalOne } from "./components/modals/Modal";
import appStyles from "./App.module.scss";
import headerStyles from "./components/header/Header.module.scss";
import mainStyles from "./components/main/Main.module.scss";
import windowStyles from "./components/modals/Modal.module.scss";



function App() {
  const [isModalOneOpen, setModalOneOpen] = useState(false);
  const [isOverlayOpen, setOverlayOpen] = useState(false);
  const [autoCloseModal, setAutoCloseModal] = useState(null);
  const [coloredStarsCount, setColoredStarsCount] = useState(
    parseInt(localStorage.getItem("coloredStarsCount")) ||
    0
  );
  const [busketCount, setbusketCount] = useState(
    parseInt(localStorage.getItem("busketCount")) || 0
  );

  // 
  // open window
  const openModal = () => {
    setModalOneOpen(true);
    setOverlayOpen(true);
    const timer = setTimeout(closeModal, 2000);
    setAutoCloseModal(timer);
  };

  // 
  // close window
  const closeModal = useCallback(() => {
    setModalOneOpen(false);
    setOverlayOpen(false);
    if (autoCloseModal) {
      clearTimeout(autoCloseModal);
      setAutoCloseModal(null);
    }
  }, [autoCloseModal]);

  const toggleWindow = () => {
    setOverlayOpen(!isOverlayOpen);
  };

  useEffect(() => {
    localStorage.setItem("coloredStarsCount", coloredStarsCount.toString());
    localStorage.setItem("busketCount", busketCount.toString());

    const handleOutsideClick = (event) => {
      if (
        (isModalOneOpen && !event.target.closest(".window-one"))
      ) {
        closeModal();
      }
    };

    document.addEventListener("click", handleOutsideClick);

    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, [isModalOneOpen, closeModal, coloredStarsCount, busketCount]);
  

  // 
  // Stars counter
  const handleStarClick = (productId, isColored) => {
    if (isColored) {
      setColoredStarsCount((prevCount) => prevCount - 1);
    } else {
      setColoredStarsCount((prevCount) => prevCount + 1);
    }
  };


  // 
  // Buskets counter
  const handleBusketClick = (productId, isButtonClicked) => {
    if (isButtonClicked) {
      setbusketCount((prevCount) => prevCount + 1);
    }
    else {
      // setbusketCount((prevCount) => prevCount - 1);
      setbusketCount((prevCount) => prevCount + 1);
    }
  };


  // 
  // WindowContext
  const contextDataMOdal = {
    openModal,
    coloredStarsCount,
    handleStarClick,
    handleBusketClick,
    busketCount
  }


  return (
    <div className={appStyles.container}>
      <WindowContext.Provider value={contextDataMOdal}>

        {isOverlayOpen && (<div className={appStyles.overlay} onClick={toggleWindow} />)}
        <div className={isModalOneOpen ? headerStyles.headerHidden : headerStyles.headerWrapper}>
          <Header />
        </div>
        <Main className={isModalOneOpen ? mainStyles.mainHidden : mainStyles.main}>
          {isModalOneOpen && (
            <ModalOne
              className={`window-one ${windowStyles.window} ${windowStyles.windowOne}`}
              closeButton={closeModal}
              text="Додано до кошика"
            />
          )}
        </Main>

      </WindowContext.Provider>
    </div>
  );
}


export default App;
