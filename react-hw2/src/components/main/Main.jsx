import CardsList from "../cardsList/CardsList";

export default function Main({className, children}) {

  return (
    <main className={className}>
      {children}

      <CardsList />

    </main>
  );
}
