import React, { useContext } from "react";
import { WindowContext } from "../../WindowsContext";
import {Icon} from "./icons/star/Star";
import Basket from "./icons/basket/Basket"
import styles from "./Header.module.scss";

export default function Header() {
  const { coloredStarsCount, busketCount } = useContext(WindowContext);

  return (
    <header className={styles.header}>

      <div className={styles.headlineWrapper}>
        <h1 className={styles.headline}>OLDphones</h1>
        <p className={styles.headlineText}>If you are old, but still remember</p>
      </div>

      <div className={styles.iconsWrapper}>
        <div className={styles.starWrapper}>
          <Icon/>
          <p className={styles.iconText}>{coloredStarsCount}</p>
        </div>
        <div className={styles.basketWrapper}>
          <Basket/>
          <p className={styles.iconText}>{busketCount}</p>
        </div>
      </div>
      
    </header>
    );
}
