import { ReactComponent as IconSVG } from './Icon.svg';
import { ReactComponent as IconSVGTransp } from './Icon_transparent.svg';

const Icon = () => {
    return <IconSVG />;
  };

const IconTransparent = () => {
  return <IconSVGTransp />;
};
  
export {Icon, IconTransparent};