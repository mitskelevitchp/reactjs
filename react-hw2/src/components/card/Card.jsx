import React, { useContext } from "react";
import { WindowContext } from "../../WindowsContext";
import PropTypes from "prop-types"
import { Icon, IconTransparent} from "../header/icons/star/Star";
import Button from "../button/Button";
import styles from "./Card.module.scss";
import stylesBtn from "../button/Button.module.scss";

export default function Card({ product, onStarClick, onButtonClick}) {

  // 
  // take data from WindowContext
  const { openModal, handleStarClick, handleBusketClick } = useContext(WindowContext);

  // useEffect(() => {
  // }, []);

  const handleCardStarClick = () => {
    const newIsColored = !product.isColored;
    // numbers og stars in the header
    onStarClick(product.id, newIsColored);
    handleStarClick(product.id, !newIsColored);

    const currentProducts = JSON.parse(localStorage.getItem("Selected products")) || [];
    const isProductInArray = currentProducts.some((item) => item.id === product.id);

    if (
      newIsColored
      &&
      (!isProductInArray || isProductInArray)
      ) {
      currentProducts.push(product);
      localStorage.setItem("Selected products", JSON.stringify(currentProducts));
    } else if (
      !newIsColored
      && isProductInArray
      ) {
      console.log(isProductInArray);
      console.log(currentProducts);
      const updatedProducts = currentProducts.filter((item) => item.id !== product.id);
      localStorage.setItem("Selected products", JSON.stringify(updatedProducts));
    }
  };


  const handleCardButtonClick = (event) => {
    event.stopPropagation();
    openModal();

    const newIsBusketed = !product.isBusketClicked;
    onButtonClick(product.id, newIsBusketed);
    handleBusketClick(product.id, newIsBusketed);

    const currentProducts = JSON.parse(localStorage.getItem("Products in the Busket")) || [];
    // const isProductInArray = currentProducts.some((item) => item.id === product.id);

    // if (newIsBusketed && !isProductInArray) {
      currentProducts.push(product);
      localStorage.setItem("Products in the Busket", JSON.stringify(currentProducts));
    // }
    // else if (!newIsBusketed && isProductInArray) {
      // const updatedProducts = currentProducts.filter((item) => item.id !== product.id);
      // localStorage.setItem("Products in the Busket", JSON.stringify(updatedProducts));
      // currentProducts.push(product);
      // localStorage.setItem("Products in the Busket", JSON.stringify(currentProducts));
    // }
  };


  return (
    <li className={styles.listItem}>

      <div className={styles.iconWrapper} onClick={handleCardStarClick}>
        {product.isColored ? (
          <>
            <Icon />
            <div className={styles.tooltip}>Видалити з обраного</div>
          </>
          ) : (
          <>
            <IconTransparent />
            <div className={styles.tooltip}>Додати до обраного</div>
          </>
          )}
      </div>

      <div className={styles.pictureWrapper}>
        <img src={product.url} alt={`смартфон ${product.name} фото`} className={styles.picture} />
      </div>
      <h2 className={styles.headline}>{product.name}</h2>
      <p className={styles.text}>{`Артикул: ${product.article}`}</p>
      <p className={styles.text}>{`Колір: ${product.color}`}</p>
      <p className={styles.price}> Ціна: <span className={styles.accent}>{`${product.price}`} грн</span></p>
      <div className={styles.buttonWrapper}>
        <Button className={`${stylesBtn.button} ${styles.button}`} text={"Додати в кошик"} onClick={handleCardButtonClick} />
      </div>
      
    </li>
  );
}


// 
// PropTypes
Card.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired,
  }).isRequired,
  onStarClick: PropTypes.func.isRequired,
  onButtonClick: PropTypes.func.isRequired,
};