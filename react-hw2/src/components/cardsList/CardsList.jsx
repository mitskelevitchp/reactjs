import React, { useState, useEffect } from "react";
import Card from "../card/Card";
import styles from "./CardsList.module.scss";

export default function CardsList() {
    const [products, setProducts] = useState([]);

    
    // 
    // take propucts from json
    useEffect(() => {
        fetch("./products/products.json")
          .then((response) => response.json())
          .then((data) => {
            setProducts(data);
        })
          .catch((error) => console.error(error));
        }, []);


    // 
    // add property isColored
    const handleStarClick = (productId) => {
        setProducts((prevProducts) => {
            return prevProducts.map((product) => {
            if (product.id === productId) {
                return { ...product, isColored: !product.isColored };
            }
            return product;
            });
        });
    };


    // 
    // add property isBusketClicked
    const handleBusketClick = (productId) => {
        setProducts((prevProducts) => {
            return prevProducts.map((product) => {
            if (product.id === productId) {
                return { ...product, isBusketClicked: !product.isBusketClicked };
            }
            return product;
            });
        });
    };



    return (
        <ul className={styles.list}>

            {products.map((product) => (
                <Card
                    key={product.id}
                    product={product}
                    onStarClick={() => handleStarClick(product.id)}
                    onButtonClick={() => handleBusketClick(product.id)}
                />
            ))}
            
        </ul>
    );
}