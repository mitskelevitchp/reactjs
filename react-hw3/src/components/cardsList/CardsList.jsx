import React, { useState, useEffect } from "react";
import { Card } from "../card/Card";
import styles from "./CardsList.module.scss";




export default function CardsList() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch("./products/products.json")
          .then((response) => response.json())
          .then((data) => {
            setProducts(data);
        })
          .catch((error) => console.error(error));
        }, []);

    const handleStarClick = (productId) => {
        setProducts((prevProducts) => {
            return prevProducts.map((product) => {
            if (product.id === productId) {
                return { ...product, isColored: !product.isColored };
            }
            return product;
            });
        });
    };

    const handleBusketClick = (productId) => {
        setProducts((prevProducts) => {
            return prevProducts.map((product) => {
            if (product.id === productId) {
                return { ...product, isBusketClicked: !product.isBusketClicked };
            }
            return product;
            });
        });
    };




    return (
        <div className={styles.contentWrapper}>
            <h2 className={styles.contentHeadline}>Магазин відверто вживаної техніки, що ще дихає</h2>
            <p>Культові телефони, які пам'ятають тепло людських рук. Кожен з них залишав по собі лише добрі емоції своєю якісною збіркою та гарним рівнем прийому сигналу. Комплектація моделей повна, включає зарядний пристрій, інструкцію з експлуатації на чистій мандаринській китайській та оригінально покоцану коробку.</p>
            <ul className={styles.list}>
                {products.map((product) => (
                    <Card
                    key={product.id}
                    product={product}
                    onStarClick={() => handleStarClick(product.id)}
                    onButtonClick={() => handleBusketClick(product.id)}
                    />
                ))}
            </ul>
        </div>
    );
}