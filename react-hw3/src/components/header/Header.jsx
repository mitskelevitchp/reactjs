import React, { useContext } from "react";
import Context from "../Context";
import { Link } from "react-router-dom";
import { Icon } from "./icons/star/Star";
import Basket from "./icons/basket/Basket"
import styles from "./Header.module.scss";

export default function Header() {
  const { coloredStarsCount, busketCount } = useContext(Context);

  return (
    <header className={styles.header}>

      <nav className={styles.nav}>
        <Link to="/">Головна</Link>
        <Link to="/selected">Обрані продукти</Link>
        <Link to="/busket">Кошик</Link>
      </nav>

      <div className={styles.titleWrapper}>
      <Link to="/">
        <div className={styles.headlineWrapper}>
          <h1 className={styles.headline}>OLDphones</h1>
          <p className={styles.headlineText}>If you are old, but still remember</p>
       </div>
      </Link>

      <div className={styles.iconsWrapper}>
        <Link to="/selected">
          <div className={styles.starWrapper}>
            <Icon/>
            <p className={styles.iconText}>{coloredStarsCount}</p>
          </div>
        </Link>
        <Link to="/busket">
          <div className={styles.basketWrapper}>
            <Basket/>
            <p className={styles.iconText}>{busketCount}</p>
          </div>
        </Link>
      </div>
      </div>

      
    </header>
    );
}
