import Button from "../button/Button";
import windowStyles from "./Modal.module.scss";

export function ModalOne({ className, closeButton, headline, text, actions }) {
  return (
    <div className={className}>
      <div className={windowStyles.headlineWrapper}>
        <h2 className={windowStyles.headline}>{headline}</h2>
        <div onClick={closeButton} className={windowStyles.cross}></div>
      </div>
      <p className={windowStyles.text}>{text}</p>
      <div className={windowStyles.windowButtonWrapperOne}>
        <Button
          className={`${windowStyles.button} ${windowStyles.windowButton}`}
          text="Ok"
          actions={null}
        />
        <Button
          className={`${windowStyles.button} ${windowStyles.windowButton}`}
          text="Cancel"
          onClick={closeButton}
          actions={null}
        />
      </div>
    </div>
  );
}

export function ModalTwo({ className, closeButton, headline, text, actions }) {
  return (
    <div className={className}>
      <div className={windowStyles.headlineWrapper}>
        <h2 className={windowStyles.headline}>{headline}</h2>
        <div onClick={closeButton} className={windowStyles.cross}></div>
      </div>
      <p className={windowStyles.text}>{text}</p>
      <div className={windowStyles.windowButtonWrapperTwo}>
        <Button
          className={`${windowStyles.button} ${windowStyles.windowButton}`}
          text="Go!"
          actions={null}
        />
      </div>
    </div>
  );
}
