import headerStyles from "./Header.module.scss";

export default function Header({ children }) {
  return <header className={headerStyles.header}>{children}</header>;
}
