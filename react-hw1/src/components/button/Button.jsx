export default function Button({ className, onClick, text, actions }) {
  return (
    <button className={className} onClick={onClick} actions={actions}>
      {text}
    </button>
  );
}
