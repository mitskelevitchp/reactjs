import React, { useState, useEffect, useCallback } from "react";
import Header from "./components/header/Header";
import Button from "./components/button/Button";
import { ModalOne, ModalTwo } from "./components/modals/Modal";
import Main from "./components/main/Main";
import appStyles from "./App.module.scss";
import headerStyles from "./components/header/Header.module.scss";
import buttonStyles from "./components/button/Button.module.scss";
import windowStyles from "./components/modals/Modal.module.scss";
import mainStyles from "./components/main/Main.module.scss";

function App() {
  const [isModalOneOpen, setModalOneOpen] = useState(false);
  const [isModalTwoOpen, setModalTwoOpen] = useState(false);
  const [isOverlayOpen, setOverlayOpen] = useState(false);

  const openModal = (modalNumber) => {
    if (modalNumber === 1) {
      setModalOneOpen(true);
      setModalTwoOpen(false);
    } else if (modalNumber === 2) {
      setModalOneOpen(false);
      setModalTwoOpen(true);
    }
    setOverlayOpen(true);
  };

  const closeModal = useCallback(() => {
    setModalOneOpen(false);
    setModalTwoOpen(false);
    setOverlayOpen(false);
  }, []);

  const toggleWindow = () => {
    setOverlayOpen(!isOverlayOpen);
  };

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (
        (isModalOneOpen && !event.target.closest(".window-one")) ||
        (isModalTwoOpen && !event.target.closest(".window-two"))
      ) {
        closeModal();
      }
    };

    document.addEventListener("click", handleOutsideClick);

    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, [isModalOneOpen, isModalTwoOpen, closeModal]);

  return (
    <div className={appStyles.container}>
      {isOverlayOpen && (
        <div className={appStyles.overlay} onClick={toggleWindow} />
      )}
      <div
        className={
          isModalOneOpen || isModalTwoOpen
            ? headerStyles.headerHidden
            : headerStyles.headerWrapper
        }
      >
        <Header>
          <div className={buttonStyles.wrapper}>
            <Button
              className={`${buttonStyles.button} ${buttonStyles.buttonWhite}`}
              onClick={(event) => {
                event.stopPropagation();
                openModal(1);
              }}
              text="Open first modal"
            />
            <Button
              className={`${buttonStyles.button} ${buttonStyles.buttonBlue}`}
              onClick={(event) => {
                event.stopPropagation();
                openModal(2);
              }}
              text="Open second modal"
            />
          </div>
        </Header>
      </div>
      
      <Main
        className={
          isModalOneOpen || isModalTwoOpen
            ? mainStyles.mainHidden
            : mainStyles.main
        }
      >
        {isModalOneOpen && (
          <ModalOne
            className={`window-one ${windowStyles.window} ${windowStyles.windowOne}`}
            closeButton={closeModal}
            headline="Modal 1"
            text="Content"
          />
        )}
        {isModalTwoOpen && (
          <ModalTwo
            className={`window-two ${windowStyles.window} ${windowStyles.windowTwo}`}
            closeButton={closeModal}
            headline="Modal 2"
            text="Another content"
          />
        )}
        <section>Some content</section>
      </Main>
    </div>
  );
}

export default App;
